const Course = require('../models/Course');


// Create a new course
/*
Steps:
	1. Create a new Course object using the mongoose model and the info from the request body.
	2. Save the new Course to the database.
*/
module.exports.addCourse = (reqBody) => {
	// create a variable "newCourse" and instantiate the name, descripition, price

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	// save the created object to our database
	return newCourse.save().then((course, error) => {
		// course creation is succesful or not
		if (error) {
			return false;
		} else {
			return true;
		};
	}).catch(error => error); // this is so that terminal won't stop running though error
};


// Retrieve all courses
/*
1. Retrieve all courses form database
*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => result).catch(error => error)
}


// Retrieve all ACTIVE courses
/*
Step/s:
1. Retrieve all courses with property isActive: true
*/

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	}).catch(error => error)
};


// Retrieve a specific course
// 1. Retrieve the course that matches the course ID provided from the URL

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	}).catch(error => error)
}


// UPDATE a course
/*
Steps:
	1. Create variable "updatedCourse" which will contain the info retrieved from the req.body
	2. Find and Update the course using the courseID retrieved from the req.params and the variable "updatedCourse" containing info from req.body
*/

module.exports.updateCourse = (courseId, data) => {
	// specify the fields/properties of the document to be updated.

	let updatedCourse = {
		name: data.name,
		description: data.description,
		price: data.price
	}

	// findByIdAndUpdate(document Id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
};



// Archive a course (Soft Delete) 
/*
Steps:
1. Update status of "isActive" into "false" which will no longer be displayed in the cliet whenever all active courses are retrieved.
*/
module.exports.archiveCourse = (courseId) => {
	let updateActiveField ={
		isActive: false
	};

	return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {
		if(error){
			return false
		} else {
			return true
		}
	}).catch(error => error)
}

// For Hard delete (deleting permanently) we can use the mongoose method findByIdAndRemove. 

	



















