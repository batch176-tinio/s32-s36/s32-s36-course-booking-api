// [SECTION] Dependencies and  Modules
	const User = require("../models/User"); 
	const Course = require("../models/Course"); 
	const bcrypt = require('bcrypt') 
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

// [SECTION] Environment Variables Setup
	dotenv.config();
	const salt = process.env.SALT;

// [SECTION] Functionalities [CREATE]
	// 1. Register New Account
	module.exports.register = (userData) => {
		
		let fName = userData.firstName
		let lName = userData.lastName
		let email = userData.email
		let passW = userData.password
		let mobil = userData.mobileNo
		
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, parseInt(salt)),
			mobileNo: mobil
		});
	
		return newUser.save().then((user,err) => {
			if (user) {
				return user;
			} else {
				return {message: 'Failed to Register account'};
			}; 
		});
	};


// User Authentication
/*
Steps:
1. Check the database if the user email exists
2. Compare the password provided in the login form with password stored in database.
3. Generate/return JSON web token if the user is successfully logged in, and return false if not.

*/

module.exports.loginUser = (data) => {
	// findOne method returns the first record in the collection that matches the search criteria. 
	// return User.findOne({ email: data.email }).then(result => {
	return User.findOne({ email: data.email }).then(result => {
		// User does not exist
		if (result == null) {
			return false;
		} else {
			// User exists
			// compareSync() is a method from the bcrypt to use in comparing the non encrypted password from the login and the database password. It return true or false.
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)

			// if the password match, return token
			if(isPasswordCorrect) {
				return {accessToken: auth.createAccessToken(result.toObject())} // result is the argument to user parameter to convert json to javascript object
			} else {
				return false;
			}
		}
	});

};


// [SECTION] Functionalities [RETRIEVE] the user details

/*
Steps:
1. Find the document in the databse uing user's ID
2. Reassign the password of the returned document to an empty string.
3. Return the result back to the client
*/

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		// change the value of the user's password to an empty string.
		result.password = '';
		return result;
	})
}


// Enroll registered user

/*
Enrollment steps
	1. Look for the user by its ID.
		-push the details of the course we're trying to enroll in. We'll push the details to a new enrollment subdocument in our user.
	2. Look for the course by its ID.
		-push the details of the enrollee/user who's trying to enroll. We'll push to a new enrollees subdocumnt in our course.
	3. When both saving documents are successful, we send a message to a client. true if successful, false if not.
*/

module.exports.enroll = async (req, res) => {
	// console.log("Test enroll route");
	console.log(req.user.id); // user's id from decoded token after verify()
	console.log(req.body.courseId); //course ID from request body

	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"})
	}

	// get the user's id to save the courseID inside enrollments field.

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		// add the course ID in an object and push that object into user enrollments array:

		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment)

		// save the changes made to our user document
		return user.save().then(user => true).catch(err => err.message)

		
		// if isUSerUpdated does not contain the boolean true, we will stop the process and return message to our client
		if(isUserUpdated !== true){
			return res.send({message: isUserUpdated});
		};

	});


	// Find the course ID we will need to push to our enrollee
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		// create an object which will be pushed to enrollees array/field
		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		// save the course document
		return course.save().then(course => true).catch(err => err.message);

		if(isCourseUpdated !== true) {
			return res.send({message: isUserUpdated});
		};
	});


	// send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true.

	if(isUserUpdated && isCourseUpdated) {
		return res.send({message: "Enrolled Successfully"})
	}
}


// Get user's enrollments

module.exports.getEnrollments = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err))
}




// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]















