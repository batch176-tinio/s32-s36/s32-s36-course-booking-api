// [SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	const dotenv = require("dotenv");
	const userRoutes = require("./routes/users");
	const courseRoutes = require("./routes/courses");

// [SECTION] Environment Setup
	dotenv.config();
	let account = process.env.CREDENTIALS;
	const port = process.env.PORT; 

// [SECTION] Server Setup
	const app = express();
	app.use(express.json());
	// it enables all origins/address/URL of the client request
	app.use(cors());

// [SECTION] Database Connection
	mongoose.connect(account);
	const connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Database Connected'));

// [SECTION] Backend Routes
	app.use('/users',userRoutes);
	app.use('/courses',courseRoutes);

// [SECTION] Server Gateway Response
	app.get('/', (req,res) => {
		res.send('Welcome to CourseGo123!')
	});

	app.listen(port, () => {
		console.log(`API is hosted on port ${port}.`);
	});



